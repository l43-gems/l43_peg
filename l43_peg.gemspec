require_relative 'lib/l43_peg'
version = L43Peg::VERSION

description = <<~DESCRIPTION
  A (still incomplete) PEG Library

  But useable for linear parsing of strings and list of strings (tokens)
DESCRIPTION

Gem::Specification.new do |s|
  s.name         = 'l43_peg'
  s.version      = version
  s.summary      = 'A (still incomplete) PEG Library'
  s.description  = description
  s.authors      = ["Robert Dober"]
  s.email        = 'robert.dober@gmail.com'
  s.files        = Dir.glob("lib/**/*.rb")
  s.files       += Dir.glob("bin/**/*")
  s.files       += %w[LICENSE README.md]
  s.bindir       = 'bin'
  s.homepage     = "https://gitlab.com/l43-gems/l43_peg"
  # s.executables += Dir.glob('bin/*').map { File.basename(_1) }
  s.licenses     = %w[AGPL-3.0-or-later]

  s.required_ruby_version = '>= 3.3.0'
end
# SPDX-License-Identifier: AGPL-3.0-or-later
