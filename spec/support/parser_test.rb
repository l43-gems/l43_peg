# frozen_string_literal: true

module Support
  module ParserTest
    def assert_parse_failure(parser, input, reason:)
      expect(parsed_failure(parser, input, return_all: true)).to eq(reason:, input:)
    end

    def assert_parse_success(parser, input, ast:, position: nil, rest: nil)
      expect(parsed_success(parser, input))
        .to eq(
          ast:,
          position: _mk_position(position, input),
          rest_string: _mk_rest(rest, input)
        )
    end

    def parsed_failure(parser, input, return_all: false)
      input = _mk_input(input)
      case parser.(input)
      in L43Peg::Failure => failure
        if return_all
          {input: failure.input.input, reason: failure.reason}
        else
          failure.reason
        end
      end
    end

    def parsed_success(parser, input)
      input = _mk_input(input)

      case parser.(input)
      in L43Peg::Success => success
        {
          ast: success.ast,
          position: success.position,
          rest_string: success.rest.input,
        }
      end
    end


    private

    def _mk_input(input)
      case input
      when String
        L43Peg::Input.new(input:)
      when Array
        L43Peg::Tokens.new(tokens: input)
      end
    end

    def _mk_position(position, input)
      return position if position

      case input
      when String
        [1, 1] 
      when Array
        1
      end
    end

    def _mk_rest(rest, input)
      return rest if rest

      case input
      when String
        ""
      when Array
        []
      end
    end
  end
end

RSpec.configure do |config|
  config.include Support::ParserTest
end
# SPDX-License-Identifier: Apache-2.0
