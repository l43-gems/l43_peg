# frozen_string_literal: true

module Support
  module Randomizer
    def sample(container, &blk)
      container = container.to_a
      idx = rand(container.size)
      blk ? blk.(container[idx]) :container[idx]
    end
  end
end

RSpec.configure do |config|
  config.extend Support::Randomizer, type: :random
  config.include Support::Randomizer, type: :random
end
# SPDX-License-Identifier: Apache-2.0
