# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  include L43Peg::Parsers

  describe 'rgx_parser' do
    context 'one letter rgx parser' do
      let(:alnum_parser) { rgx_parser('[[:alnum:]]') }

      it 'parses an x' do
        assert_parse_success(alnum_parser, 'x', ast: 'x')
      end

      it 'does not parse a %' do
        expect(parsed_failure(alnum_parser, '%')).to eq("input does not match /\\A[[:alnum:]]/ (in rgx_parser(\"[[:alnum:]]\"))")
      end

      it 'does not parse an empty input' do
        expect(parsed_failure(alnum_parser, '')).to eq("input does not match /\\A[[:alnum:]]/ (in rgx_parser(\"[[:alnum:]]\"))")
      end
    end

    context 'captures' do
      describe 'name_parser' do
        let(:name_parser) { rgx_parser("[[:blank:]]*([[:alpha:]][_[:alnum:]]+)", name: "name_parser") }

        it 'discards leading ws' do
          assert_parse_success(name_parser, " hello_42", ast: "hello_42")
        end
        it "does not need it though" do
          assert_parse_success(name_parser, "hello_42!", ast: "hello_42", rest: '!')
        end
        it 'fails on a leading digit' do
          reason = "input does not match /\\A[[:blank:]]*([[:alpha:]][_[:alnum:]]+)/ (in name_parser)"
          assert_parse_failure(name_parser, "42", reason:)
        end
      end

      describe '2nd capture' do
        let(:g_parser) { rgx_parser("[[:blank:]]*([[:alpha:]])([[:digit:]])", capture: 2) } 
        it 'gets us the 2nd' do
          assert_parse_success(g_parser, "  a3", ast: "3")
        end
      end

      describe 'capture everything' do
        let(:all_parser) { rgx_parser("(.)(.)(.)", capture: :all) }
        it 'gets us all' do
          assert_parse_success(all_parser, "abc", ast: %w[abc a b c])
        end
      end
    end

    context 'knowing what you are doing' do
      let(:rgx_rgx_parser) { rgx_parser(%r{\A X (Y) }x, capture: :all) }
        it 'gets us all' do
          assert_parse_success(rgx_rgx_parser, "XY", ast: %w[XY Y])
        end
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
