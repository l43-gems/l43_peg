# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  include L43Peg::Parsers

  describe 'token_parser' do
    context 'equality' do
      let(:parser) { token_parser("hello") }

      it 'fails on empty' do
        assert_parse_failure(parser, [], reason: "token nil does not match \"hello\" (in token_parser(\"hello\"))")
      end

      it 'fails on hell' do
        assert_parse_failure(parser, %w[hell], reason: "token \"hell\" does not match \"hello\" (in token_parser(\"hello\"))")
      end

      it 'succeeds on hello' do
        assert_parse_success(parser, %w[hello], ast: "hello")
      end

      it 'succeeds on hello world' do
        assert_parse_success(parser, %w[hello world], ast: "hello", rest: %w[world])
      end
    end

    context 'named parser equality failure' do
      let(:hello_parser) { token_parser("hello", name: "hello_parser") }

      it 'fails on hell world' do
        reason = "token \"hell\" does not match \"hello\" (in hello_parser)"
        assert_parse_failure(hello_parser, %w[hell world], reason:)
      end
    end

    context 'matching' do
      let(:parser) { token_parser(%r{\Ah(e)(l).*}) }

      it 'fails on empty' do
        reason = "token nil does not match /\\Ah(e)(l).*/ (in token_parser(/\\Ah(e)(l).*/))"
        assert_parse_failure(parser, [], reason:)
      end

      it 'fails on Hell' do
        reason = "token \"Hell\" does not match /\\Ah(e)(l).*/ (in token_parser(/\\Ah(e)(l).*/))"
        assert_parse_failure(parser, %w[Hell], reason:)
      end
      
      it 'default ast is whole match' do
        assert_parse_success(parser, %w[hello world], ast: "hello", rest: %w[world])
      end
    end

    context 'capturing a part' do
      let(:parser) { token_parser(%r{\Ah(e)(l).*}, capture: 2) }
      it 'captures second group' do
        assert_parse_success(parser, %w[hello world], ast: "l", rest: %w[world])
      end
    end

    context 'capturing everything' do
      let(:parser) { token_parser(%r{\Ah(e)(l).*}, capture: :all) }
      it 'captures second group' do
        assert_parse_success(parser, %w[hello], ast: %w[hello e l])
      end
    end

    context 'bad spec' do
      it 'raises an Argument error' do
        message = '42 must be a regular expression or a string'
        expect { token_parser(42) }.to raise_error(ArgumentError, message)
      end
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
