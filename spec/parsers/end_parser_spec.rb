# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  describe 'end_parser' do
    let(:end_parser) { L43Peg::Parsers.end_parser }
    it 'can succeed' do
      expect(parsed_success(end_parser, '')).to eq(ast: nil, position: [1, 1], rest_string: '')
    end
    it 'can also fail' do
      expect(parsed_failure(end_parser, 'alpha')).to eq("not at end of input")
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
