# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  include L43Peg::Parsers

  describe 'verb_parser' do
    context 'one letter verb parser' do
      let(:x_parser) { verb_parser('x') }

      it 'parses an x' do
        expect(parsed_success(x_parser, 'x')).to eq(ast: 'x', position: [1, 1], rest_string: '')
      end

      it 'does not parse a y' do
        expect(parsed_failure(x_parser, 'y')).to eq("input does not start with \"x\" (in verb_parser(\"x\"))")
      end

      it 'does not parse an empty input' do
        expect(parsed_failure(x_parser, '')).to eq("input does not start with \"x\" (in verb_parser(\"x\"))")
      end
    end
    context 'longer verb parser' do
      let(:def_parser) { verb_parser('def', name: 'my verb parser') }
      it 'parses "def"' do
        expect(parsed_success(def_parser, 'def')).to eq(ast: 'def', position: [1, 1], rest_string: '')
      end
      it 'parses "defp"' do
        expect(parsed_success(def_parser, 'defp')).to eq(ast: 'def', position: [1, 1], rest_string: 'p')
      end
      it 'does not parse "depp"' do
        expect(parsed_failure(def_parser, 'depp')).to eq("input does not start with \"def\" (in my verb parser)")
      end
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
