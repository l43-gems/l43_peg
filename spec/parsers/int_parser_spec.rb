# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  include L43Peg::Parsers

  describe 'int_parser' do
    context 'minimal' do
      it 'parses one char' do
        expect(parsed_success(int_parser, '0')).to eq(ast: 0, position: [1, 1], rest_string: '')
      end
      it 'or more' do
        expect(parsed_success(int_parser, '1764')).to eq(ast: 1764, position: [1, 1], rest_string: '')
      end
      it 'but not all' do
        expect(parsed_success(int_parser, '42x')).to eq(ast: 42, position: [1, 1], rest_string: 'x')
      end
    end

    context 'there is a sign on the wall' do
      it '+ is not needed, but...' do
        expect(parsed_success(int_parser, '+42x')).to eq(ast: 42, position: [1, 1], rest_string: 'x')
      end
      it 'it does not hurt, even before 0' do
        expect(parsed_success(int_parser, '+0x')).to eq(ast: 0, position: [1, 1], rest_string: 'x')
      end
      it 'the same goes for -' do
        expect(parsed_success(int_parser, '-00')).to eq(ast: 0, position: [1, 1], rest_string: '')
      end
      it 'but can make the difference (pun intended)' do
        expect(parsed_success(int_parser, '-09')).to eq(ast: -9, position: [1, 1], rest_string: '')
      end
    end

    context 'not everything is a number' do
      it "not the end, but that's the end of that" do
        expect(parsed_failure(int_parser, '')).to eq("many many(char_parser(#<Set: {\"0\", \"1\", \"2\", \"3\", \"4\", \"5\", \"6\", \"7\", \"8\", \"9\"}>)) should match at least 1 times but did only 0 times in (int_parser)")
      end
      it "nor is fourtytwo" do
        expect(parsed_failure(int_parser, 'fourtytwo')).to eq("many many(char_parser(#<Set: {\"0\", \"1\", \"2\", \"3\", \"4\", \"5\", \"6\", \"7\", \"8\", \"9\"}>)) should match at least 1 times but did only 0 times in (int_parser)")
      end
    end

    context "poor man's property testing", type: :random do
      10.times do |n|
        describe "random value ##{n.succ}" do
          sample(1..100) do |rand_value|
            it "can be parsed as positive +#{rand_value}" do
              expect(parsed_success(int_parser, "+#{rand_value}")).to eq(ast: rand_value, position: [1, 1], rest_string: '')
            end
            it "can be parsed as positive #{rand_value}" do
              expect(parsed_success(int_parser, "#{rand_value}")).to eq(ast: rand_value, position: [1, 1], rest_string: '')
            end
            it "can be parsed as negative -#{rand_value}" do
              expect(parsed_success(int_parser, "-#{rand_value}")).to eq(ast: -rand_value, position: [1, 1], rest_string: '')
            end
          end
        end
      end

    end
  end

end
# SPDX-License-Identifier: Apache-2.0
