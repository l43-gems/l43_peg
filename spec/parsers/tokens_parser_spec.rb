# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  include L43Peg::Parsers

  describe 'tokens_parser' do
    let :token_defs do
      {
        start: "start:(.+)",
        end: "--end=(.+)",
      }
    end
    let(:arg_parser) { tokens_parser(token_defs, name: "arg_parser") }

    it 'can parse start arg' do
      assert_parse_success(arg_parser, %w[start:one1], ast: {start: "one1"})
    end
    it 'can parse end arg' do
      assert_parse_success(arg_parser, %w[--end=two2], ast: {end: "two2"})
    end
    it 'can also fail' do
      reason = "no token matches (in arg_parser)"
      assert_parse_failure(arg_parser, %w[no-match], reason:)
    end
  end

  describe 'errors' do

    it 'raises an Argument error' do
      message = '42 must be a regular expression or a string'
      expect { tokens_parser(42) }.to raise_error(ArgumentError, message)
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
