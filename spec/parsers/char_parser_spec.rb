# frozen_string_literal: true

RSpec.describe L43Peg::Parser do

  include L43Peg::Parsers

  describe 'char_parser' do
    context 'one char parser' do
      let(:x_parser) { char_parser('x') }

      it 'parses an x' do
        expect(parsed_success(x_parser, 'x')).to eq(ast: 'x', position: [1, 1], rest_string: '')
      end

      it 'does not parse a y' do
        expect(parsed_failure(x_parser, 'y')).to eq("char \"y\"")
      end

      it 'does not parse an empty input' do
        expect(parsed_failure(x_parser, '')).to eq("cannot parse at end of input char_parser(#<Set: {\"x\"}>)")
      end
    end

    context 'char sets' do
      let(:ary_parser) { char_parser(['a', 'b']) }
      let(:set_parser) { char_parser(Set.new(['a', 'b'])) }

      it 'parses from an ary' do
        expect(parsed_failure(ary_parser, '')).to eq("cannot parse at end of input char_parser(#<Set: {\"a\", \"b\"}>)")
      end

      it 'parses from a set' do
        expect(parsed_failure(set_parser, '')).to eq("cannot parse at end of input char_parser(#<Set: {\"a\", \"b\"}>)")
      end
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
