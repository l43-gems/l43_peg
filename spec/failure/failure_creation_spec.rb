# frozen_string_literal: true

RSpec.describe L43Peg::Failure do

  describe "creation" do
    it "has some default values" do
      failure = described_class.new
      expect(failure.parsed_by).to be_nil
      expect(failure.position).to eq([1, 1])
      expect(failure.reason).to eq("")
      expect(failure.input).to be_nil
      expect(failure.cache).to be_nil
    end

    it "can take different values too" do
      cache = L43Peg::Cache.new
      input = L43Peg::Input.new
      failure = described_class.new(
        cache:, input:,
        parsed_by: "my parser",
        position: [7, 3],
        reason: "no idea"
      )
      expect(failure.parsed_by).to eq("my parser")
      expect(failure.position).to eq([7, 3])
      expect(failure.reason).to eq("no idea")
      expect(failure.cache).to eq(cache)
      expect(failure.input).to eq(input)
    end
  end
  
end
# SPDX-License-Identifier: Apache-2.0
