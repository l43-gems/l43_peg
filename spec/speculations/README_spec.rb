# DO NOT EDIT!!!
# This file was generated from "README.md" with the speculate_about gem, if you modify this file
# one of two bad things will happen
# - your documentation specs are not correct
# - your modifications will be overwritten by the speculate command line
# YOU HAVE BEEN WARNED
RSpec.describe "README.md" do
  # README.md:12
  context "`arg_parser`" do
    # README.md:16
    include L43Peg::Combinators
    let :args_spec do
    {
    start: "--start=(.*)",
    end: "(?:--end|-e)=(.*)",
    kwd: "--(alpha|beta|gamma)"
    }
    end
    # README.md:29
    let(:parser) { args_parser(args_spec) }
    it "we can parse some input (README.md:35)" do
      assert_parse_success(parser, %w[--start=42 --beta -e=44], ast: {start: "42", kwd: "beta", end: "44"}, rest: [])
    end
    it "we can get the rest in a list of tokens (README.md:41)" do
      assert_parse_success(parser, %w[--start=42 --beta -e=44 -s=not_an_arg --end=too_late], ast: {start: "42", kwd: "beta", end: "44"}, rest: %w[-s=not_an_arg --end=too_late])
    end
    it "note that multiple values are passed into an array (README.md:47)" do
      input = %w[--end=42 --beta -e=44 --beta --end=not_too_late --gamma]
      ast   = {end: %w[42 44 not_too_late], kwd: %w[beta beta gamma]}
      assert_parse_success(parser, input, ast:, rest: [])
    end
    # README.md:53
    context "Postprocessing" do
      # README.md:57
      let :int_args do
      {
      start: "--start=(.*)",
      end: "--end=(.*)",
      inc: "--inc=(.*)"
      }
      end
      let(:int_arg_parser) {args_parser(int_args, name: "int parser", &:to_i)}
      it "we can convert the string valus (README.md:70)" do
        assert_parse_success(int_arg_parser, %w[--start=42 --end=44 --inc=2], ast: {start: 42, end: 44, inc: 2}, rest: [])
      end
    end
  end
end
