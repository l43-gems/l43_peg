# frozen_string_literal: true

RSpec.describe L43Peg::Cache do
  
  context "creation" do
    it "can be created with defaults" do
      cache = described_class.new

      expect(cache.cache).to be_kind_of(Hash)
      expect(cache.cache).to be_empty
    end
    
    it "can be created with a cache" do
      inner_cache = {a: 1}
      cache = described_class.new(cache: inner_cache)

      expect(cache.cache).to eq(a: 1)
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
