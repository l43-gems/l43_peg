# frozen_string_literal: true

RSpec.describe L43Peg::Parser do
  describe "creation" do
    let(:parser) do
      described_class.new("success_parser") do |input, _cache, _name|
        L43Peg::Success.new(rest: input, position: input.position, ast: :ok)
      end
    end

    it "needs name and function" do
      expect(parser.name).to eq("success_parser")
      expect(parser.fn).to be_kind_of(Proc)
    end

    it "returns success and the input as rest" do
      input = L43Peg::Input.new(input: "abc")
      parser.(input, cache: nil) => L43Peg::Success(ast: :ok, position: [1, 1], rest: ^input)
    end
  end
  
end
# SPDX-License-Identifier: Apache-2.0
