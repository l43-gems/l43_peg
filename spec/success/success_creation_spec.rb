# frozen_string_literal: true

RSpec.describe L43Peg::Success do

  context "creation" do
    it "can be created with defaults" do
      success = described_class.new

      expect(success.ast).to be_nil
      expect(success.cache).to be_nil
      expect(success.position).to eq([1, 1])
      expect(success.rest).to be_nil
    end

    it "can be created with values" do
      ast   = Object.new
      cache = L43Peg::Cache.new
      rest  = L43Peg::Input.new
      success = described_class.new(
        ast:, cache:, position: [6, 2], rest:
      )

      expect(success.ast).to eq(ast)
      expect(success.cache).to eq(cache)
      expect(success.position).to eq([6, 2])
      expect(success.rest).to eq(rest)
    end
  end
  
end
# SPDX-License-Identifier: Apache-2.0
