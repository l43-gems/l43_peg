# frozen_string_literal: true

class SubjectClass
  extend L43::OpenObject
  attributes :a, b: 1
end

RSpec.describe Class do
  describe 'equality with required arguments' do
    let(:subject1) { SubjectClass.new(a: "a") }
    let(:subject2) { SubjectClass.new(a: "a") }
    let(:subject3) { SubjectClass.new(a: "b") }

    it 'subject1 == subject2' do
      expect(subject1).to eq(subject2)
    end

    it 'subject1 != subject2' do
      expect(subject1).not_to eq(subject3)
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
