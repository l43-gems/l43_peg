# frozen_string_literal: true

RSpec.describe L43Peg::Combinators do

  include L43Peg::Combinators

  describe 'args_parser' do
    let :token_defs do
      {
        start: "start:(.+)",
        end: "end:(.+)",
        by: %r{\A(?:-b|--by)=(.*)}
      }
    end
    let(:parser) { args_parser(token_defs, name: "args_parser") }

    it 'can parse start arg' do
      assert_parse_success(parser, %w[start:one1], ast: {start: "one1"})
    end
    it 'can parse end arg' do
      assert_parse_success(parser, %w[end:two2], ast: {end: "two2"})
    end
  end

  describe 'multiple values and rest' do
    let :token_defs do
      {
        a: "a:(.*)",
        b: "b:(.*)",
      }
    end

    let(:parser) { args_parser(token_defs, name: "multiple") }

    it 'parses ok' do
      assert_parse_success(parser, %w[a:1 b:2 b:3 a:4 c], ast: {a: %w[1 4], b: %w[2 3]}, rest: %w[c])
    end

    it 'multiple values are flat' do
      assert_parse_success(parser, %w[a:1 b:2 a:3 a:4], ast: {a: %w[1 3 4], b: "2"})
    end
    
  end

  describe 'errors' do
    it 'raises an Argument error' do
      message = '42 must be a regular expression or a string'
      expect { args_parser(42) }.to raise_error(ArgumentError, message)
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
