# frozen_string_literal: true

RSpec.describe L43Peg::Combinators do
  include described_class
  include L43Peg::Parsers

  describe 'many' do

    context 'counts and failures' do
      let(:many_failures) { many(failure_parser) }
      # one_success can never succeed
      let(:one_success) { many(failure_parser, min: 1) }

      it 'does not fail if min is 0' do
        parsed_success(many_failures, "abc") => {ast:, rest_string: rest}
        expect(ast).to eq([])
        expect(rest).to eq('abc')
      end
      it 'does not fail if min is 0, even on an empty string' do
        parsed_success(many_failures, '') => {ast:, rest_string: rest}
        expect(ast).to eq([])
        expect(rest).to eq('')
      end
      it 'fails however if min is > 0' do
        expect(parsed_failure(one_success, "abc")).to eq('many many(failure_parser) should match at least 1 times but did only 0 times')
      end
    end

    context 'counts and parsing characters' do
      describe 'greedy' do
        let(:greedy) { many(char_parser) }

        it 'parses everything' do
          parsed_success(greedy, 'abc') => {ast:, rest_string: rest}
          expect(ast).to eq(%w[a b c])
          expect(rest).to eq('')
        end

        it 'parses everything, even if everything is notyhing' do
          parsed_success(greedy, '') => {ast:, rest_string: rest}
          expect(ast).to eq([])
          expect(rest).to eq('')
        end
      end

      describe 'lazy' do
        let(:lazy) { many(char_parser, max: 1) }

        it 'parses only one' do
          parsed_success(lazy, 'abc') => {ast:, rest_string: rest}
          expect(ast).to eq(%w[a])
          expect(rest).to eq('bc')
        end
        it 'but does not need one' do
          parsed_success(lazy, '') => {ast:, rest_string: rest}
          expect(ast).to eq([])
          expect(rest).to eq('')
        end
      end

      describe 'demanding' do
        let(:demanding) { many(char_parser, min: 2) }
        it 'parses everything' do
          parsed_success(demanding, 'abc') => {ast:, rest_string: rest}
          expect(ast).to eq(%w[a b c])
          expect(rest).to eq('')
        end

        it "one's just not 'nuff" do
          expect(parsed_failure(demanding, 'a')).to eq("many many(char_parser()) should match at least 2 times but did only 1 times")
        end
        
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
