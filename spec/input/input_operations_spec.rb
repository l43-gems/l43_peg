# frozen_string_literal: true

RSpec.describe L43Peg::Input do

  context "position" do
    it "is 1, 1 at the beginning" do
      input = described_class.new(input: "abc\ndef")
      expect(input.position).to eq([1, 1])
    end
    it "can be set" do
      input = described_class.new(col: 2, lnb: 3)
      expect(input.position).to eq([2, 3])
    end
  end

  context "drop" do
    let(:input) { described_class.new(input: "xy\nz") }
    it 'drops one char by default' do
      expect(input.drop).to eq(described_class.new(input: "y\nz", col: 2))
    end
    it 'keeps a correct line count' do
      expect(input.drop(3)).to eq(described_class.new(input: "z", lnb: 2))
    end
    it 'keeps a correct line and col count' do
      expect(input.drop(4)).to eq(described_class.new(input: "", col: 2, lnb: 2))
    end
    it 'can also drop strings' do
      expect(input.drop('abcd')).to eq(described_class.new(input: "", col: 2, lnb: 2))
    end
  end

end
# SPDX-License-Identifier: Apache-2.0
