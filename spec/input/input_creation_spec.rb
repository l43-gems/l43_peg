# frozen_string_literal: true

RSpec.describe L43Peg::Input do

  describe "creation" do
    it "has some default values" do
      input = described_class.new

      expect(input.col).to eq(1)
      expect(input.context).to eq({})
      expect(input.input).to eq("")
      expect(input.lnb).to eq(1)
    end

    it "or can be created with values" do
      input = described_class.new(
        input: "hello",
        col: 2,
        lnb: 3,
        context: {a: 1}
      )

      expect(input.col).to eq(2)
      expect(input.context).to eq(a: 1)
      expect(input.input).to eq("hello")
      expect(input.lnb).to eq(3)
    end
  end
  
end
# SPDX-License-Identifier: Apache-2.0
