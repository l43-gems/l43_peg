# L43Peg

## A Parse Expression Grammar library for Ruby

### This Version (v0.1.x) is Alpha Quality (many PEG features are missing, like recursion and even alternatives.

It is however released because it offers quite some nice parsing of ARGV which shall be demonstrated by the following
[speculations](https://rubygems.org/gems/speculate_about)

See [README_spec.rb](spec/speculations/README_spec.rb) for the generated code for details

### Context: `arg_parser`

Given the following argument specification

```ruby 
  include L43Peg::Combinators
   let :args_spec do
       {
          start: "--start=(.*)",
          end: "(?:--end|-e)=(.*)",
          kwd: "--(alpha|beta|gamma)"
       }
   end
```

And the assoicated parser

```ruby
   let(:parser) { args_parser(args_spec) } 
```

Then we can parse some input

```ruby
    assert_parse_success(parser, %w[--start=42 --beta -e=44], ast: {start: "42", kwd: "beta", end: "44"}, rest: [])
```

And we can get the rest in a list of tokens

```ruby
    assert_parse_success(parser, %w[--start=42 --beta -e=44 -s=not_an_arg --end=too_late], ast: {start: "42", kwd: "beta", end: "44"}, rest: %w[-s=not_an_arg --end=too_late])
```

Also note that multiple values are passed into an array

```ruby
    input = %w[--end=42 --beta -e=44 --beta --end=not_too_late --gamma]
    ast   = {end: %w[42 44 not_too_late], kwd: %w[beta beta gamma]}
    assert_parse_success(parser, input, ast:, rest: [])
```

#### Context: Postprocessing

When we map the parser

```ruby
   let :int_args do
     {
      start: "--start=(.*)",
      end: "--end=(.*)",
      inc: "--inc=(.*)"
     }
   end
   let(:int_arg_parser) {args_parser(int_args, name: "int parser", &:to_i)}
```

Then we can convert the string valus

```ruby
    assert_parse_success(parser, %w[--start=42 --end=44 --inc=2], ast: {start: 42, end: 44, inc: 2}, rest: [])
```


## Author

Copyright © 2024 Robert Dober
robert.dober@gmail.com

# LICENSE

GNU AFFERO GENERAL PUBLIC LICENSE, Version 3, 19 November 2007. Please refer to [LICENSE](LICENSE) for details.

<!-- SPDX-License-Identifier: AGPL-3.0-or-later -->
