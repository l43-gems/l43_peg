# frozen_string_literal: true

require_relative 'l43/require_helper'
require_relative 'l43/open_object'
require_subdir {}

module L43Peg
  VERSION = "0.1.2"
end
# SPDX-License-Identifier: AGPL-3.0-or-later
