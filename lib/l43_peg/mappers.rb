# frozen_string_literal: true

module L43Peg
  module Mappers

    def join_and_to_i
      -> list do
        list.join.to_i
      end
    end
    
    def join_maps
      -> maps do
        maps.reduce Hash.new do |map, entry|
          map.merge(entry) { |*args| args.drop(1).flatten }
        end
      end
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
