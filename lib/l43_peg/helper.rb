# frozen_string_literal: true

module L43Peg
  module Helper
    def fail_parser(reason)
      L43Peg::Failure.new(reason:)
    end
    def succeed_parser(ast, input, cache: nil)
      L43Peg::Success.new(ast:, rest: input, cache: cache || input.cache)
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
