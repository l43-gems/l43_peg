# frozen_string_literal: true

module L43Peg
  class Input
    extend L43::OpenObject

    attributes col: 1, input: "", lnb: 1, context: {}
    
    def drop(by=nil)
      case by
      when nil
        _drop_by_n(1)
      when String
        _drop_by_n(by.length)
      else
        _drop_by_n(by)
      end
    end

    def empty? = input.empty?

    def position = [@col, @lnb]

    private

    def _drop_by_n(n)
      return self if input.empty?
      _split(n) => col, lnb, head, tail
      self.class.new(input: tail, context:, col:, lnb:)
    end

    # Very inefficent but sufficent for my use cases so far
    # and convenient for regex parsers
    def _split(n)
      head = input.slice(0...n)
      lines = head.count("\n")
      tail = input.slice(n..-1) || ''
      new_col = 
        if lines.zero?
          col + n
        else
          1 + head.sub(%r{\A.*\n}m, "").length
        end
      [new_col, lnb + lines, head, tail]
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
