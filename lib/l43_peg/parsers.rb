# frozen_string_literal: true

require_relative 'combinators'
require_subdir {}

module L43Peg
  module Parsers extend self
    def args_parser(args, name: nil)      = L43Peg::Parsers::ArgsParser.new(args, name:)
    def char_parser(charset = nil)        = L43Peg::Parsers::CharParser.new(charset)
    def end_parser                        = L43Peg::Parsers::EndParser.instance
    def failure_parser                    = L43Peg::Parsers::FailureParser.instance
    def int_parser                        = L43Peg::Parsers::IntParser.instance
    def rgx_parser(rgx, name: nil, **o)   = L43Peg::Parsers::RgxParser.new(rgx, name:, **o)
    def token_parser(spc, name: nil, **o) = L43Peg::Parsers::TokenParser.new(spc, name:, **o)
    def tokens_parser(map, name: nil, &b) = L43Peg::Parsers::TokensParser.new(map, name:, &b)
    def verb_parser(verb, name: nil)      = L43Peg::Parsers::VerbParser.new(verb, name:)
  end
end
# SPDX-License-Identifier: Apache-2.0
