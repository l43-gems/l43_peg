# frozen_string_literal: true

module L43Peg
  class Parser

    attr_reader :fn, :name

    def call(input, cache: L43Peg::Cache.new) = fn.(input, cache, name)

    private

    def initialize(name, &fn)
      @name = name
      @fn = fn
    end
    
  end
end
# SPDX-License-Identifier: Apache-2.0
