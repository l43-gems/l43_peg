# frozen_string_literal: true

module L43Peg
  class Tokens
    extend L43::OpenObject

    attributes tnb: 1, tokens: [], context: {}
    
    def drop(by=nil)
      by = by || 1
      return self if empty?
      self.class.new(tokens: input.drop(by), context:, tnb: tnb + by)
    end

    def empty? = tokens.empty?
    def head = tokens.first
    def input = tokens

    def match(str_or_rgx, option)
      return if empty?

      case str_or_rgx
      when String
        head == str_or_rgx && head
      when Regexp
        _match_rgx(str_or_rgx, option || 0)
      end
    end

    def position = tnb

    private

    def _match_rgx(rgx, option)
      md = rgx.match(head)
      return unless md

      if option == :all
        md.to_a
      else
        md[option]
      end
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
