# frozen_string_literal: true

module L43Peg
  module Parsers
    class CharParser < L43Peg::Parser

      private

      def initialize(charset=nil)
        charset = _mk_set(charset)
        super("char_parser(#{charset})") do |input, cache, name|
          if input.input.empty?
            L43Peg::Failure.new(cache:, input:, parsed_by: self, reason: "cannot parse at end of input #{name}")
          elsif charset.nil? || charset.member?(input.input[0])
            L43Peg::Success.new(ast: input.input[0], cache:, rest: input.drop, position: input.position)
          else
            L43Peg::Failure.new(cache:, input:, parsed_by: self, reason: "char #{input.input[0].inspect}")
          end
        end
      end

      def _mk_set(charset)
        return unless charset
        case charset
        when String
          Set.new(charset.split(""))
        when Set
          charset
        else
          Set.new(charset)
        end
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
