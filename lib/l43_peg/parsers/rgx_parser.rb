# frozen_string_literal: true

module L43Peg
  module Parsers
    class RgxParser < L43Peg::Parser

      private

      def initialize(rgx, name: nil, **options)
        name = name || "rgx_parser(#{rgx.inspect})"
        rgx = _mk_rgx(rgx)
        super(name) do |input, cache, _name|
          case rgx.match(input.input)
          in MatchData => md
            ast = _from_match(md, options) 
            L43Peg::Success.new(ast:, cache:, rest: input.drop(md[0]), position: input.position)
          else
            L43Peg::Failure.new(cache:, input:, parsed_by: self, reason: "input does not match #{rgx.inspect} (in #{name})")
          end
        end
      end

      def _from_match(md, options)
        case options[:capture]
        in NilClass
          md[1] || md[0]
        in Integer => n
          md[n]
        in :all
          md.to_a
        end
      end

      def _mk_rgx(rgx)
        case rgx
        when String
          Regexp.compile("\\A#{rgx}")
        else
         rgx
        end
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
