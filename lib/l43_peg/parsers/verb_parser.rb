# frozen_string_literal: true

module L43Peg
  module Parsers
    class VerbParser < L43Peg::Parser

      private

      def initialize(verb, name: nil)
        name = name || "verb_parser(#{verb.inspect})"
        super(name) do |input, cache, _name|
          if input.input.start_with?(verb)
            L43Peg::Success.new(ast: verb, cache:, rest: input.drop(verb), position: input.position)
          else
            L43Peg::Failure.new(cache:, input:, parsed_by: self, reason: "input does not start with #{verb.inspect} (in #{name})")
          end
        end
      end

    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
