# frozen_string_literal: true

module L43Peg
  module Parsers
    class FailureParser < L43Peg::Parser

      def self.instance
        @__instance__ ||= new
      end

      private

      def initialize
        super('failure_parser') do |input, cache, _name=nil|
          L43Peg::Failure.new(cache:, input:, parsed_by: self, reason: "this parser always fails")
        end
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
