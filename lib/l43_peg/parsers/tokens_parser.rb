# frozen_string_literal: true

module L43Peg
  module Parsers
    class TokensParser < L43Peg::Parser

      private

      def initialize(map, name: nil, &blk)
        map = _check_arg!(map)
        name = name || "tokens_parser(#{map.inspect})"
        super(name) do |tokens, cache, _name|
          case _find_matching(map, tokens)
          in [token, success]
            _succeed(tokens:, token:, success:, &blk)
          else
            reason = "no token matches (in #{name})"
            L43Peg::Failure.new(cache:, input: tokens, parsed_by: self, reason:)
          end
        end

      end

      def _check_arg!(map)
        case map
        when Hash
          _mk_tokens(map)
        else
          raise ArgumentError, "#{map.inspect} must be a regular expression or a string"
        end
      end

      def _find_matching(map, tokens)
        map.each do |token, token_parser|
          case token_parser.(tokens)
          in L43Peg::Success => success
            return [token, success]
          else
          end
        end
        nil
      end

      def _mk_tokens(map)
        map.map do |token, spec|
          [token, TokenParser.new(Regexp.compile(spec), capture: 1)]
        end.to_h
      end

      def _succeed(success:, token:, tokens:, &blk)
        result = if blk
                   blk.(success.ast)
                 else
                   success.ast
                 end
        L43Peg::Success.new(ast: {token => result}, cache: success.cache, rest: success.rest, position: tokens.position)
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
