# frozen_string_literal: true

require_relative '../parsers'

module L43Peg
  module Parsers
    class IntParser < L43Peg::Parser
      extend L43Peg::Combinators
      extend L43Peg::Mappers

      class << self
        def instance
          @__instance__ ||= _mk_int_parser
        end

        private

        def _mk_int_parser
          map(seq(many(Parsers.char_parser("+-"), max: 1), many(Parsers.char_parser("0".."9"), min: 1), name: "int_parser"), &join_and_to_i)
        end
      end

    end
  end
end
# SPDX-License-Identifier: Apache-2.0
