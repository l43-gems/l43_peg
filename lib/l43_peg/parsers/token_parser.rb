# frozen_string_literal: true

module L43Peg
  module Parsers
    class TokenParser < L43Peg::Parser

      private

      def initialize(token_spec, name: nil, **options)
        _check_arg!(token_spec)
        name = name || "token_parser(#{token_spec.inspect})"
        super(name) do |tokens, cache, _name|
          if result = tokens.match(token_spec, options[:capture])
            L43Peg::Success.new(ast: result, cache:, rest: tokens.drop(1), position: tokens.position)
          else
            reason = "token #{tokens.head.inspect} does not match #{token_spec.inspect} (in #{name})"
            L43Peg::Failure.new(cache:, input: tokens, parsed_by: self, reason:)
          end
        end
      end

      def _check_arg!(token_spec)
        case token_spec
        when Regexp, String
        else
          raise ArgumentError, "#{token_spec.inspect} must be a regular expression or a string"
        end
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
