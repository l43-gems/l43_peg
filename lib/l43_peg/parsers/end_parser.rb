# frozen_string_literal: true

require_relative '../parsers'

module L43Peg
  module Parsers
    class EndParser < L43Peg::Parser

      def self.instance
        @__instance__ ||= new
      end

      private

      def initialize
        super('end_parser') do |input, cache, _name=nil|
          if input.input.empty?
            L43Peg::Success.new(cache:, rest: input, position: input.position)
          else
            L43Peg::Failure.new(cache:, input:, parsed_by: self, reason: "not at end of input")
          end
        end
      end
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
