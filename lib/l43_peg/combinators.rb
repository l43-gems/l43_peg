# frozen_string_literal: true

require_relative 'helper'
require_relative 'mappers'
require_relative 'parser'
require_relative 'parsers'
require_subdir('combinators') {}

module L43Peg
  module Combinators extend self

    include L43Peg::Helper
    include L43Peg::Mappers
    include L43Peg::Parsers

    def args_parser(definitions, name: nil, &block)
      parser = tokens_parser(definitions, &block)
      name   = name || "args_parser(#{definitions.inspect})"
      inner  = many(parser)
      map(inner, name:, fn: join_maps)
    end

    def many(parser, name: nil, min: 0, max: nil)
      Parser.new(name || "many(#{parser.name})") {|input, cache, name1=nil| Many.many(input:, cache:, name: name1 || name, parser:, min:, max:)}
    end
    
    def map(parser, name: nil, fn: nil, &mapper)
      raise ArgumentError, "must not provide keyword parameyer fn and a block" if fn && mapper
      mapper = fn || mapper
      raise ArgumentError, "must provide keyword parameyer fn or a block" unless mapper
      Parser.new(name || "map(#{parser.name})") {|input, cache, name=nil| _map(input:, cache:, name:, parser:, mapper:)}
    end

    def seq(*parsers, name: nil)
      name ||= "seq(#{parsers.map(&:name).join(", ")})"
      Parser.new(name) {|input, cache, _name=nil| Seq.seq(input:, cache:, name:, parsers:)}
    end

    private

    def _map(input:, cache:, name:, parser:, mapper:)
      case parser.(input, cache:)
      in L43Peg::Failure => failure
        failure
      in L43Peg::Success => success
        success.map(&mapper)
      end
    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
