# frozen_string_literal: true

module L43Peg
  class Failure
    extend L43::OpenObject
    attributes cache: nil, input: nil, parsed_by: nil, position: [1, 1], reason: ""
  end
end
# SPDX-License-Identifier: Apache-2.0
