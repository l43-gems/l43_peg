# frozen_string_literal: true

module L43Peg
  class Success
    def _init
      return if @position
      @position = _position
    end
    extend L43::OpenObject

    attributes ast: nil, cache: nil, position: nil, rest: nil

    def map(&mapper)
      self.class.new(ast: mapper.(ast), cache:, position:, rest:)
    end

    private


    def _position
      case @rest
      when Tokens
        1
      else
        [1, 1]
      end
    end
  end
end
# SPDX-License-Identifier: Apache-2.0
