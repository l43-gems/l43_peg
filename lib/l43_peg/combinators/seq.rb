# frozen_string_literal: true

require_relative "../helper"
module L43Peg
  module Combinators
    module Seq extend self

      include L43Peg::Helper
      def seq(input:, cache:, name:, parsers:)
        curr_input = input
        curr_cache = cache
        ast = []
        parsers.each do |parser|
          case parser.(curr_input, cache: curr_cache)
          in L43Peg::Failure => failure
            return L43Peg::Failure.new(reason: "#{failure.reason} in (#{name})", position: failure.position, input:)
          in L43Peg::Success => success
            ast.push(success.ast)
            curr_input = success.rest
            curr_cache = success.cache
          end
        end
        succeed_parser(ast, curr_input, cache: curr_cache) 
      end

    end
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
