# frozen_string_literal: true

require_relative "../helper"
module L43Peg
  module Combinators
    module Many extend self

      include L43Peg::Helper
      def many(input:, cache:, name:, parser:, min:, max:)
        curr_input = input
        curr_cache = cache
        count = 0
        ast   = []

        loop do
          if max && count == max
            return succeed_parser(ast, curr_input, cache: curr_cache)
          end
          case parser.(curr_input, cache: curr_cache)
          in L43Peg::Success => success
            ast.push(success.ast)
            curr_input = success.rest
            curr_cache = success.cache
            count += 1
          in L43Peg::Failure
            if count < min
              return fail_parser("many #{name} should match at least #{min} times but did only #{count} times")
            end
            return succeed_parser(ast, curr_input, cache: curr_cache)
          end
        end
      end

    end
  end
end

# SPDX-License-Identifier: AGPL-3.0-or-later
