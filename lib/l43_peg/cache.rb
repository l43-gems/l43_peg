# frozen_string_literal: true

module L43Peg
  class Cache
    extend L43::OpenObject
    attributes cache: {}
  end
end
# SPDX-License-Identifier: Apache-2.0
