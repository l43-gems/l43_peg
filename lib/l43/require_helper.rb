# frozen_string_literal: true

class Object
  def require_subdir(path=nil, &blk)
    Dir.glob(File.join([File.dirname(blk.source_location.first), path, '/**/*.rb'].compact)).each { require _1}
  end
end
# SPDX-License-Identifier: AGPL-3.0-or-later
