# frozen_string_literal: true

class MatchData
  def deconstruct(*) = to_a
end
# SPDX-License-Identifier: AGPL-3.0-or-later
